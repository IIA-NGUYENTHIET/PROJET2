package escampe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import java.util.*;

import javax.swing.text.PlainDocument;

import escampe.EscampeBoard.Pawn;
import iia.modele.CoupJeu;
import iia.modele.joueur.*;
import iia.modele.PlateauJeu;

public class EscampeBoardTest {

    // Instance variables
    private EscampeBoard escampeBoard;

    @Before
    public void init() {
        this.escampeBoard = new EscampeBoard();
    }

    @Test
    public void partialTestBoardIntegrity() {
        assertEquals(3, this.escampeBoard.getCell(0, 1));
        assertEquals(2, this.escampeBoard.getCell(5, 5));
        assertEquals(1, this.escampeBoard.getCell(1, 3));
    }

    @Test
    public void setAndGetPawn() {
        this.escampeBoard.setPawn(1, 1, Pawn.BLACK_LICORNE);
        this.escampeBoard.setPawn(1, 2, Pawn.WHITE_LICORNE);
        this.escampeBoard.setPawn(2, 1, Pawn.WHITE_PALADIN);
        assertEquals(Pawn.BLACK_LICORNE, this.escampeBoard.getPawn(1, 1));
        assertEquals(Pawn.WHITE_LICORNE, this.escampeBoard.getPawn(1, 2));
        assertEquals(Pawn.WHITE_PALADIN, this.escampeBoard.getPawn(2, 1));
    }

    @Test
    public void validMoveTestsStandard() {
        // Black pawns
        this.escampeBoard.setPawn(1, 1, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(0, 1, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(2, 1, Pawn.BLACK_LICORNE);

        // White pawns
        this.escampeBoard.setPawn(1, 0, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(1, 2, Pawn.WHITE_LICORNE);

        // Test a player takes a pawn that isnt theirs
        assertFalse(this.escampeBoard.isValidMove("B2-C2", "blanc"));

        // Take an enemy licorn is OK
        assertTrue(this.escampeBoard.isValidMove("B2-B3", "noir"));

        // Take a paladin is not OK
        assertFalse(this.escampeBoard.isValidMove("B2-B1", "noir"));
        assertFalse(this.escampeBoard.isValidMove("B2-A2", "noir"));

        // Take a friend licorn is not OK
        assertFalse(this.escampeBoard.isValidMove("B2-C2", "noir"));

        // Not the right amount of move
        this.escampeBoard.setPawn(5, 5, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("F6-F5", "noir"));

        // Good amount of move
        assertTrue(this.escampeBoard.isValidMove("F6-F4", "noir"));

        // Path partially blocked but too many moves
        this.escampeBoard.setPawn(5, 4, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("F6-F4", "noir"));

        // Path completely blocked
        this.escampeBoard.setPawn(4, 5, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("F6-D6", "noir"));

        // From case 3 it's possible to move to next if path is free
        this.escampeBoard.setPawn(1, 4, Pawn.BLACK_PALADIN);
        // but not if it's diagonial
        assertFalse(this.escampeBoard.isValidMove("B5-C6", "noir"));

        // From case 3 it's possible to move to next if path is free
        assertTrue(this.escampeBoard.isValidMove("B5-C5", "noir"));
        assertTrue(this.escampeBoard.isValidMove("B5-A5", "noir"));
        assertTrue(this.escampeBoard.isValidMove("B5-B4", "noir"));
        assertTrue(this.escampeBoard.isValidMove("B5-B6", "noir"));

        // If there's one person, and not on the same side, still ok
        this.escampeBoard.setPawn(2, 5, Pawn.BLACK_PALADIN);
        assertTrue(this.escampeBoard.isValidMove("B5-B6", "noir"));
        assertTrue(this.escampeBoard.isValidMove("B5-C5", "noir"));

        // If there's two person, not ok
        this.escampeBoard.setPawn(0, 5, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("B5-B6", "noir"));
        assertTrue(this.escampeBoard.isValidMove("B5-C5", "noir"));
        this.escampeBoard.setPawn(3, 4, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(3, 3, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("D5-C5", "noir"));
        assertFalse(this.escampeBoard.isValidMove("D5-E5", "noir"));

        // On the side not ok either
        this.escampeBoard.setPawn(5, 2, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(4, 2, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("F3-F2", "noir"));
        assertFalse(this.escampeBoard.isValidMove("F3-F4", "noir"));

        // On the side and ok
        this.escampeBoard.setPawn(4, 2, null);
        this.escampeBoard.setPawn(4, 1, Pawn.BLACK_PALADIN);
        assertTrue(this.escampeBoard.isValidMove("F3-F4", "noir"));

        // Set the last cell played on
        this.escampeBoard.setLastCell(1);

        // If the edging does not correspond, invalid move
        assertFalse(this.escampeBoard.isValidMove("A2-A5", "noir"));
        // valid move otherwise
        assertTrue(this.escampeBoard.isValidMove("B2-B3", "noir"));

        // Can't go over a licorn
        this.escampeBoard.setPawn(0, 2, Pawn.BLACK_PALADIN);
        assertFalse(this.escampeBoard.isValidMove("A3-D3", "noir"));

        // Licorn can't take other licorn
        this.escampeBoard.setPawn(4, 0, Pawn.BLACK_LICORNE);
        this.escampeBoard.setPawn(5, 0, Pawn.WHITE_LICORNE);
        assertFalse(this.escampeBoard.isValidMove("E1-F1", "noir"));

        // Licorn can move elsewhere
        assertTrue(this.escampeBoard.isValidMove("E1-D1", "noir"));

        // Skip turn
        assertTrue(this.escampeBoard.isValidMove("E", "noir"));
    }

    @Test
    public void validMoveTestsStart() {

        // Good start
        assertTrue(this.escampeBoard.isValidMove("C6/A6/B5/D5/E6/F6", "noir"));
        // One pawn on the other side
        assertFalse(this.escampeBoard.isValidMove("C1/A6/B5/D5/E6/F6", "noir"));

        // All corners except one and good
        assertTrue(this.escampeBoard.isValidMove("A6/A5/B6/B5/C5/F6", "noir"));

        // All corners except one and bad
        assertFalse(this.escampeBoard.isValidMove("A6/A5/B6/B5/C3/F6", "noir"));

        // Duplicates
        assertFalse(this.escampeBoard.isValidMove("F6/A5/B6/B5/A6/A6", "noir"));

        // All in corners and bad start
        assertFalse(this.escampeBoard.isValidMove("A6/A5/B6/B5/E1/F1", "noir"));
        // All in corners and good start
        assertTrue(this.escampeBoard.isValidMove("A6/A5/B6/B5/E6/F6", "noir"));

        // No vertical start
        assertFalse(this.escampeBoard.isValidMove("A1/A2/A3/A4/A5/A6", "noir"));

        // Enemy cant be on the same side
        this.escampeBoard.setPawn(0, 0, Pawn.WHITE_LICORNE);
        this.escampeBoard.setPawn(1, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(2, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(4, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(4, 0, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(5, 0, Pawn.WHITE_PALADIN);

        // Same side but don't touch
        assertFalse(this.escampeBoard.isValidMove("A2/B1/C1/D1/D2/F2", "noir"));

        // Same side and touch
        assertFalse(this.escampeBoard.isValidMove("A1/B1/C1/D1/D2/F2", "noir"));

        // Other side is ok
        assertTrue(this.escampeBoard.isValidMove("A5/B5/C5/D5/E5/F5", "noir"));
    }

    @Test
    public void testCopy() {
        this.escampeBoard.setPawn(0, 0, Pawn.WHITE_LICORNE);
        this.escampeBoard.setPawn(1, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(2, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(4, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(4, 0, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(5, 0, Pawn.WHITE_PALADIN);
        EscampeBoard eb = (EscampeBoard) this.escampeBoard.copy();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                assertTrue(eb.getPawn(i, j) == (this.escampeBoard.getPawn(i, j)));
            }
        }

        assertTrue(this.escampeBoard.getPawn(3, 3) == null);
    }

    @Test
    public void testDistance() {
        Coord p1 = new Coord(0, 0);
        Coord p2 = new Coord("B2");
        Coord p3 = new Coord(-1, -1);
        Coord p4 = new Coord("A2");
        assertEquals(0, this.escampeBoard.distance_manhattan(p1, p1));
        assertEquals(2, this.escampeBoard.distance_manhattan(p1, p2));
        assertEquals(this.escampeBoard.distance_manhattan(p1, p3), this.escampeBoard.distance_manhattan(p1, p2));
        assertEquals(1, this.escampeBoard.distance_manhattan(p1, p4));
    }

    @Test
    public void getPawns() {
        this.escampeBoard.setPawn(0, 0, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(1, 1, Pawn.BLACK_LICORNE);
        this.escampeBoard.setPawn(0, 2, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(3, 3, Pawn.WHITE_PALADIN);
        ArrayList<Coord> all = this.escampeBoard.getAllCoordOfPawn(Pawn.WHITE_PALADIN);
        assertEquals(3, all.size());
        Coord a = all.get(0);
        Coord b = all.get(1);
        Coord c = all.get(2);
        assertEquals(0, a.x);
        assertEquals(0, a.y);
        assertEquals(0, b.x);
        assertEquals(2, b.y);
        assertEquals(3, c.x);
        assertEquals(3, c.y);
    }

    @Test
    public void getLicorne() {
        this.escampeBoard.setPawn(0, 0, Pawn.WHITE_LICORNE);
        this.escampeBoard.setPawn(0, 2, Pawn.BLACK_LICORNE);

        Coord white = this.escampeBoard.getLicorneCoord("blanc");
        Coord black = this.escampeBoard.getLicorneCoord("noir");

        assertEquals(0, white.x);
        assertEquals(0, white.y);
        assertEquals(0, black.x);
        assertEquals(2, black.y);
    }

    @Test
    public void getPaladin() {
        this.escampeBoard.setPawn(0, 0, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(1, 1, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(2, 2, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(3, 0, Pawn.WHITE_PALADIN);
        this.escampeBoard.setPawn(1, 0, Pawn.WHITE_LICORNE);
        this.escampeBoard.setPawn(5, 5, Pawn.WHITE_PALADIN);

        ArrayList<Coord> coords_black = this.escampeBoard.getAllPaladinCoords("noir");
        assertEquals(0, coords_black.size());
        ArrayList<Coord> coords_white = this.escampeBoard.getAllPaladinCoords("blanc");
        assertEquals(5, coords_white.size());
        Coord a = coords_white.get(0);
        Coord b = coords_white.get(1);
        Coord c = coords_white.get(2);
        Coord d = coords_white.get(3);
        Coord e = coords_white.get(4);

        assertEquals(0, a.x);
        assertEquals(1, b.x);
        assertEquals(2, c.x);
        assertEquals(3, d.x);
        assertEquals(5, e.x);

        assertEquals(0, a.y);
        assertEquals(1, b.y);
        assertEquals(2, c.y);
        assertEquals(0, d.y);
        assertEquals(5, e.y);
    }

    @Test
    public void testHashCodeAndEquals() {
        this.escampeBoard.init_zobrist();
        this.escampeBoard.setPawn(1, 1, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(2, 1, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(3, 1, Pawn.BLACK_PALADIN);
        this.escampeBoard.setPawn(4, 1, Pawn.BLACK_PALADIN);
        this.escampeBoard.setJoueur(new Joueur("Blanc"), new Joueur("Noir"));

        EscampeBoard eb1 = (EscampeBoard) this.escampeBoard.copy();
        EscampeBoard eb2 = (EscampeBoard) eb1.copy();

        CoupEscampe c = new CoupEscampe("B2-B3");
        assertEquals(eb1.hashCode(), this.escampeBoard.hashCode());
        assertEquals(eb1, this.escampeBoard);
        this.escampeBoard.play(c.getContent(), "noir");
        eb1.play(c.getContent(), "noir");
        assertEquals(eb1.hashCode(), this.escampeBoard.hashCode());
        assertEquals(eb1, this.escampeBoard);
        eb2.setPawn(1, 1, null);
        eb2.setPawn(1, 2, Pawn.BLACK_PALADIN);
        assertNotEquals(eb2.hashCode(), eb1.hashCode());
        assertNotEquals(eb2, eb1);
        assertNotEquals(eb2.hashCode(), this.escampeBoard.hashCode());
        assertNotEquals(eb2, this.escampeBoard);
        eb2.setLastCell(3);
        assertEquals(eb2.hashCode(), eb1.hashCode());
        assertEquals(eb2, eb1);
        assertEquals(eb2.hashCode(), this.escampeBoard.hashCode());
        assertEquals(eb2, this.escampeBoard);

        Hashtable<PlateauJeu, String> htbl = new Hashtable<>();
        htbl.put(eb1, "OK");
        assertEquals(htbl.get(eb1), htbl.get(eb2));
        assertEquals(htbl.get(eb2), htbl.get(this.escampeBoard));
        assertEquals(htbl.get(eb1), htbl.get(this.escampeBoard));
    }
}
