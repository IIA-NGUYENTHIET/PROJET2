package iia.modele;

import java.io.Serializable;

public class TranspositionUnit implements Serializable {
    public int prof;
    public double val;

    public enum Flag {
        EXACT, BINF, BSUP;
    };

    public Flag flag;

    public TranspositionUnit(int prof, double val, Flag flag) {
        this.prof = prof;
        this.val = val;
        this.flag = flag;
    }

    @Override
    public String toString() {
        return prof + "/" + val + "/" + flag;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (!(o instanceof TranspositionUnit)) {
            return false;
        }

        TranspositionUnit other = (TranspositionUnit) o;

        return this.prof == other.prof && this.val == other.val && this.flag == other.flag;
    }

}