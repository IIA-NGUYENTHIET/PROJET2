package iia.modele.joueur;

import java.io.Serializable;

public class Joueur implements Serializable //A
{

    private String id;

    public Joueur(String s)
    {
        id = s;
    }

    public String getId()    // A
    {
        return id;
    }

    public void setId(String s)   //A(String)
    {
        id = s;
    }

    public String toString()
    {
        return id;
    }

}
