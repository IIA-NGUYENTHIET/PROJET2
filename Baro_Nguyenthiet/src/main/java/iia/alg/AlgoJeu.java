package iia.alg;

import iia.modele.CoupJeu;
import iia.modele.PlateauJeu;
import iia.modele.joueur.*;

public interface AlgoJeu {

    /**
     * Renvoie le meilleur
     * 
     * @param p
     * @return
     */
    public CoupJeu meilleurCoup(PlateauJeu p, long time);

    public int getNbNoeuds();

    public int getNbFeuilles();

    public Joueur getJMax();

    public Joueur getJMin();

}
