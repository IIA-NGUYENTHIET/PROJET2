/**
 * 
 */

package iia.alg;

import java.util.ArrayList;
import java.util.HashMap;

import iia.modele.CoupJeu;
import iia.modele.PlateauJeu;
import iia.modele.TranspositionUnit;
import iia.modele.joueur.Joueur;

public class AlphaBeta implements AlgoJeu {

    /**
     * La profondeur de recherche par défaut
     */
    private final static int PROFMAXDEFAUT = 4;

    // -------------------------------------------
    // Attributs
    // -------------------------------------------

    /**
     * La profondeur de recherche utilisée pour l'algorithme
     */
    private int profMax = PROFMAXDEFAUT;

    /**
     * L'heuristique utilisée par l'algorithme
     */
    private Heuristique h;

    /**
     * Le joueur Min (l'adversaire)
     */
    private Joueur joueurMin;

    /**
     * Le joueur Max (celui dont l'algorithme de recherche adopte le point de vue)
     */
    private Joueur joueurMax;

    /**
     * Le nombre de noeuds développé par l'algorithme (intéressant pour se faire une
     * idée du nombre de noeuds développés)
     */
    private int nbnoeuds;

    /**
     * Le nombre de feuilles évaluées par l'algorithme
     */
    private int nbfeuilles;

    /**
     * Temps passé
     */
    private long timeSpentPossibleMove = 0;
    private long timeSpentTotal = 0;

    // -------------------------------------------
    // Constructeurs
    // -------------------------------------------
    public AlphaBeta(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
        this(h, joueurMax, joueurMin, PROFMAXDEFAUT);
    }

    public AlphaBeta(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
        this.h = h;
        this.joueurMin = joueurMin;
        this.joueurMax = joueurMax;
        profMax = profMaxi;
        // System.out.println("Initialisation d'un MiniMax de profondeur " + profMax);
    }

    // -------------------------------------------
    // Méthodes de l'interface AlgoJeu
    // -------------------------------------------
    public CoupJeu meilleurCoup(PlateauJeu p, long time) {
        long start = System.currentTimeMillis();
        ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMax);
        this.timeSpentPossibleMove += System.currentTimeMillis() - start;
        double alpha = -Heuristique.INFINITY;
        double beta = Heuristique.INFINITY;
        double best = alpha;
        CoupJeu mc = cp.get(0);

        if (cp.size() == 1) {
            return mc;
        }
        for (CoupJeu c : cp) {
            PlateauJeu tmp = p.copy();
            tmp.joue(this.joueurMax, c);
            alpha = -negab(tmp, 1, -beta, -alpha, -1);

            // We already know the path that will lead to victory
            if (alpha == Heuristique.INFINITY) {
                return c;
            }

            if (alpha > best) {
                best = alpha;
                mc = c;
            }
        }
        this.timeSpentTotal += System.currentTimeMillis() - start;
        return mc;
    }

    // -------------------------------------------
    // Méthodes publiques
    // -------------------------------------------
    public String toString() {
        return "AlphaBeta(ProfMax=" + profMax + ")";
    }

    public Joueur getJMax() {
        return this.joueurMax;
    }

    public Joueur getJMin() {
        return this.joueurMin;
    }

    // -------------------------------------------
    // Méthodes internes
    // -------------------------------------------

    private double negab(PlateauJeu p, int prof, double alpha, double beta, int parite) {
        if (p.finDePartie() || prof >= this.profMax) {
            this.nbfeuilles++;
            if (parite == 1) {
                return this.h.eval(p, this.joueurMax) * parite;
            } else {
                return this.h.eval(p, this.joueurMin) * parite;
            }
        } else {
            this.nbnoeuds++;
            ArrayList<CoupJeu> cp;
            long start = System.currentTimeMillis();
            if (parite == 1) {
                cp = p.coupsPossibles(this.joueurMax);
            } else {
                cp = p.coupsPossibles(this.joueurMin);
            }
            this.timeSpentPossibleMove += System.currentTimeMillis() - start;
            for (CoupJeu c : cp) {
                PlateauJeu tmp = p.copy();
                if (parite == 1) {
                    tmp.joue(this.joueurMax, c);
                } else {
                    tmp.joue(this.joueurMin, c);
                }

                alpha = Math.max(alpha, -negab(tmp, prof + 1, -beta, -alpha, -parite));

                if (alpha >= beta) {
                    return beta;
                }
            }
            return alpha;
        }
    }

    public int getNbNoeuds() {
        return this.nbnoeuds;
    }

    public int getNbFeuilles() {
        return this.nbfeuilles;
    }

    public long getTimeSpentPossibleMove() {
        return this.timeSpentPossibleMove;
    }

    public long getTimeSpentTotal() {
        return this.timeSpentTotal;
    }

}
