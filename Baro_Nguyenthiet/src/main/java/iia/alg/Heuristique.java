package iia.alg;

import iia.modele.PlateauJeu;
import iia.modele.joueur.Joueur;

public interface Heuristique {
	public static double INFINITY = 999999999;
	public double eval(PlateauJeu p, Joueur j);

}
 