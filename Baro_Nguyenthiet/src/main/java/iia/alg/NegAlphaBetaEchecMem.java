/**
 * 
 */

package iia.alg;

import java.util.*;

import iia.modele.*;
import iia.modele.TranspositionUnit.Flag;
import iia.modele.joueur.Joueur;

public class NegAlphaBetaEchecMem implements AlgoJeu {

    /**
     * La profondeur de recherche par défaut
     */
    private final static int PROFMAXDEFAUT = 4;

    // -------------------------------------------
    // Attributs
    // -------------------------------------------

    /**
     * La profondeur de recherche utilisée pour l'algorithme
     */
    private int profMax = PROFMAXDEFAUT;

    /**
     * L'heuristique utilisée par l'algorithme
     */
    private Heuristique h;

    /**
     * Le joueur Min (l'adversaire)
     */
    private Joueur joueurMin;

    /**
     * Le joueur Max (celui dont l'algorithme de recherche adopte le point de vue)
     */
    private Joueur joueurMax;

    /**
     * Le nombre de noeuds développé par l'algorithme (intéressant pour se faire une
     * idée du nombre de noeuds développés)
     */
    private int nbnoeuds;

    /**
     * Le nombre de feuilles évaluées par l'algorithme
     */
    private int nbfeuilles;

    /**
     * Temps passé
     */
    private long timeSpentPossibleMove = 0;
    private long timeSpentTotal = 0;

    /**
     * Table de transposition
     */
    private HashMap<PlateauJeu, TranspositionUnit> tt;

    // -------------------------------------------
    // Constructeurs
    // -------------------------------------------
    public NegAlphaBetaEchecMem(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
        this(h, joueurMax, joueurMin, PROFMAXDEFAUT);
    }

    public NegAlphaBetaEchecMem(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
        this.h = h;
        this.joueurMin = joueurMin;
        this.joueurMax = joueurMax;
        this.profMax = profMaxi;
        this.tt = new HashMap<>();
        // System.out.println("Initialisation d'un MiniMax de profondeur " + profMax);

    }

    // -------------------------------------------
    // Méthodes de l'interface AlgoJeu
    // -------------------------------------------
    public CoupJeu meilleurCoup(PlateauJeu p, long time_allowed) {
        long start = System.currentTimeMillis();
        ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMax);
        this.timeSpentPossibleMove += System.currentTimeMillis() - start;
        double alpha = -Heuristique.INFINITY;
        double beta = Heuristique.INFINITY;
        double best = alpha;
        CoupJeu mc = cp.get(0);
        boolean out_of_time = false;
        int prof;
        for (prof = 2; prof < this.profMax && !out_of_time; prof++) {
            if (cp.size() == 1) {
                return mc;
            }
            for (CoupJeu c : cp) {
                PlateauJeu tmp = p.copy();
                tmp.joue(this.joueurMax, c);
                alpha = -alphabeta(tmp, 1, prof, -beta, -alpha, -1);

                if (alpha == Heuristique.INFINITY) {
                    return c;
                }

                if (alpha > best) {
                    best = alpha;
                    mc = c;
                }
            }
            long now = System.currentTimeMillis();
            System.out.println(now - start);
            System.out.println(prof);
            out_of_time = now - start >= time_allowed;
        }
        this.timeSpentTotal += System.currentTimeMillis() - start;
        return mc;
    }

    // -------------------------------------------
    // Méthodes publiques
    // -------------------------------------------
    public String toString() {
        return "NegAlphaBetaEchecMem(ProfMax=" + profMax + ")";
    }

    public Joueur getJMax() {
        return this.joueurMax;
    }

    public Joueur getJMin() {
        return this.joueurMin;
    }

    public HashMap<PlateauJeu, TranspositionUnit> getTt() {
        return this.tt;
    }

    public void setTt(HashMap<PlateauJeu, TranspositionUnit> tt) {
        this.tt = tt;
    }

    public void setH(Heuristique h) {
        this.h = h;
    }

    // -------------------------------------------
    // Méthodes internes
    // -------------------------------------------

    private double mtd(PlateauJeu p, double f) {
        double g = f;
        double min = -Heuristique.INFINITY;
        double max = Heuristique.INFINITY;
        double beta;
        while (min < max) {
            if (Math.abs(g - min) < 0.01) {
                beta = g + 1;
            } else {
                beta = g;
            }
            g = -alphabeta(p, 2, this.profMax, beta - 1, beta, 1);
            if (g < beta) {
                max = g;
            } else {
                min = g;
            }
        }
        return g;
    }

    private double alphabeta(PlateauJeu p, int prof, int profMaxi, double alpha, double beta, int parite) {
        double alpha_init = alpha;
        TranspositionUnit entree_t = this.tt.get(p);
        if (entree_t != null && entree_t.prof < prof) {
            if (entree_t.flag == TranspositionUnit.Flag.EXACT) {
                return entree_t.val;
            } else if (entree_t.flag == TranspositionUnit.Flag.BINF) {
                alpha = Math.max(alpha, entree_t.val);
            } else if (entree_t.flag == TranspositionUnit.Flag.BSUP) {
                beta = Math.min(beta, entree_t.val);
            }
            if (alpha >= beta) {
                return entree_t.val;
            }
        }

        double max = -Heuristique.INFINITY;
        if (p.finDePartie() || prof >= profMaxi) {
            this.nbfeuilles++;
            if (parite == 1) {
                return this.h.eval(p, this.joueurMax) * parite;
            } else {
                return this.h.eval(p, this.joueurMin) * parite;
            }
        } else {
            this.nbnoeuds++;
            ArrayList<CoupJeu> cp;
            long start = System.currentTimeMillis();

            if (parite == 1) {
                cp = p.coupsPossibles(this.joueurMax);
            } else {
                cp = p.coupsPossibles(this.joueurMin);
            }
            this.timeSpentPossibleMove += System.currentTimeMillis() - start;
            for (CoupJeu c : cp) {
                PlateauJeu tmp = p.copy();
                if (parite == 1) {
                    tmp.joue(this.joueurMax, c);
                } else {
                    tmp.joue(this.joueurMin, c);
                }

                max = Math.max(max, -alphabeta(tmp, prof + 1, profMaxi, -beta, -alpha, -parite));

                if (max > alpha) {
                    alpha = max;
                }

                if (alpha >= beta) {
                    break;
                }
            }
        }

        TranspositionUnit.Flag flag;
        if (max <= alpha_init) {
            flag = Flag.BSUP;
        } else if (max >= beta) {
            flag = Flag.BINF;
        } else {
            flag = Flag.EXACT;
        }
        TranspositionUnit new_entree_t = new TranspositionUnit(prof, max, flag);
        this.tt.put(p, new_entree_t);
        return max;
    }

    public int getNbNoeuds() {
        return this.nbnoeuds;
    }

    public int getNbFeuilles() {
        return this.nbfeuilles;
    }

    public long getTimeSpentPossibleMove() {
        return this.timeSpentPossibleMove;
    }

    public long getTimeSpentTotal() {
        return this.timeSpentTotal;
    }
}
