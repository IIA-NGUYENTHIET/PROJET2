package escampe;

import iia.modele.CoupJeu;

public class CoupEscampe implements CoupJeu {
    String content;

    public CoupEscampe(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    @Override
    public int hashCode() {
        return this.content.hashCode();
    }

    @Override
    public String toString() {
        return this.content;
    }

    public CoupJeu fromString(String s) {
        return new CoupEscampe(s);
    }
}