package escampe.heuristique;

import java.util.ArrayList;

import escampe.*;
import escampe.EscampeBoard.Pawn;
import iia.alg.Heuristique;
import iia.modele.*;
import iia.modele.joueur.Joueur;

public class HeuristiqueSup {
    public static double coeff_cp = 1;
    public static double coeff_E = 20;
    public static double coeff_sum_dist = 3;
    public static double coeff_aggresive = 10;
    public static double coeff_defensive = 1;
    public static double coeff_cp_coord = 2;

    public void set_coeff(double a, double b, double c, double d, double e, double f) {
        coeff_cp = a;
        coeff_E = b;
        coeff_sum_dist = c;
        coeff_aggresive = d;
        coeff_defensive = e;
        coeff_cp_coord = f;
    }

    public static Heuristique hbb = new Heuristique() {

        public double eval(PlateauJeu p, Joueur j) {
            EscampeBoard board = (EscampeBoard) p;
            int player_coef;
            if (board.isJb(j)) {
                player_coef = 1;
            } else {
                player_coef = -1;
            }

            if (!board.getBLA()) {
                return Heuristique.INFINITY;
            } else if (!board.getWLA()) {
                return -Heuristique.INFINITY;
            }
            ArrayList<CoupJeu> cp = board.coupsPossibles(j);
            double cp_count = player_coef * cp.size();

            if (cp_count == 1) {
                String coup = ((CoupEscampe) cp.get(0)).getContent();
                // If we have to pass, it's very bad
                if (coup.equals("E")) {
                    cp_count = -HeuristiqueSup.coeff_E * player_coef;
                }
            }

            Coord coord_licorne_white = board.getLicorneCoord("blanc");
            Coord coord_licorne_black = board.getLicorneCoord("noir");
            ArrayList<Coord> coords_paladin_white = board.getAllPaladinCoords("blanc");
            ArrayList<Coord> coords_paladin_black = board.getAllPaladinCoords("noir");
            double sum_dist_black_to_white_licorne = 0;
            double sum_dist_white_to_black_licorne = 0;

            // The two that will attack
            Coord aggresive_white = coords_paladin_white.get(0);
            Coord second_aggresive_white = coords_paladin_white.get(1);
            Coord aggresive_black = coords_paladin_black.get(0);
            Coord second_aggresive_black = coords_paladin_black.get(1);

            // The two that will defend
            Coord defensive_white = coords_paladin_white.get(2);
            Coord second_defensive_white = coords_paladin_white.get(3);
            Coord defensive_black = coords_paladin_black.get(2);
            Coord second_defensive_black = coords_paladin_black.get(3);

            // The coordinator
            Coord coordinator_white = coords_paladin_white.get(4);
            Coord coordinator_black = coords_paladin_black.get(4);

            for (Coord coord_black : coords_paladin_black) {
                int dist_to_enemy = board.distance_manhattan(coord_black, coord_licorne_white);
                int dist_to_friend = board.distance_manhattan(coord_black, coord_licorne_black);
                int dist_aggresive_black = board.distance_manhattan(aggresive_black, coord_licorne_white);
                int dist_second_aggresive_black = board.distance_manhattan(second_aggresive_black, coord_licorne_white);
                int dist_defensive_black = board.distance_manhattan(defensive_black, coord_licorne_black);
                int dist_second_defensive_black = board.distance_manhattan(second_defensive_black, coord_licorne_black);

                if (dist_to_enemy < dist_aggresive_black) {
                    coordinator_black = second_aggresive_black;
                    second_aggresive_black = aggresive_black;
                    aggresive_black = coord_black;
                } else if (dist_to_enemy < dist_second_aggresive_black) {
                    coordinator_black = second_aggresive_black;
                    second_aggresive_black = coord_black;
                } else if (dist_to_friend < dist_defensive_black) {
                    coordinator_black = second_defensive_black;
                    second_defensive_black = defensive_black;
                    defensive_black = coord_black;
                } else if (dist_to_friend < dist_second_defensive_black) {
                    coordinator_black = second_defensive_black;
                    second_defensive_black = coord_black;
                } else {
                    coordinator_black = coord_black;
                }

                sum_dist_black_to_white_licorne += dist_to_enemy;
            }

            for (Coord coord_white : coords_paladin_white) {
                int dist_to_enemy = board.distance_manhattan(coord_white, coord_licorne_black);
                int dist_to_friend = board.distance_manhattan(coord_white, coord_licorne_white);
                int dist_aggresive_white = board.distance_manhattan(aggresive_white, coord_licorne_black);
                int dist_second_aggresive_white = board.distance_manhattan(second_aggresive_white, coord_licorne_black);
                int dist_defensive_white = board.distance_manhattan(defensive_white, coord_licorne_white);
                int dist_second_defensive_white = board.distance_manhattan(second_defensive_white, coord_licorne_white);

                if (dist_to_enemy < dist_aggresive_white) {
                    coordinator_white = second_aggresive_white;
                    second_aggresive_white = aggresive_white;
                    aggresive_white = coord_white;
                } else if (dist_to_enemy < dist_second_aggresive_white) {
                    coordinator_white = second_aggresive_white;
                    second_aggresive_white = coord_white;
                } else if (dist_to_friend < dist_defensive_white) {
                    coordinator_white = second_defensive_white;
                    second_defensive_white = defensive_white;
                    defensive_white = coord_white;
                } else if (dist_to_friend < dist_second_defensive_white) {
                    coordinator_white = second_defensive_white;
                    second_defensive_white = coord_white;
                } else {
                    coordinator_white = coord_white;
                }

                sum_dist_white_to_black_licorne += dist_to_enemy;
            }
            double dist_aggresive_black = board.distance_manhattan(aggresive_black, coord_licorne_white);
            double dist_second_aggresive_black = board.distance_manhattan(second_aggresive_black, coord_licorne_white);
            double dist_defensive_black = board.distance_manhattan(defensive_black, coord_licorne_black);
            double dist_second_defensive_black = board.distance_manhattan(second_defensive_black, coord_licorne_black);
            double dist_aggresive_white = board.distance_manhattan(aggresive_white, coord_licorne_black);
            double dist_second_aggresive_white = board.distance_manhattan(second_aggresive_white, coord_licorne_black);
            double dist_defensive_white = board.distance_manhattan(defensive_white, coord_licorne_white);
            double dist_second_defensive_white = board.distance_manhattan(second_defensive_white, coord_licorne_white);

            double val_cp = (cp_count * HeuristiqueSup.coeff_cp);
            double val_sum_dist = HeuristiqueSup.coeff_sum_dist
                    * ((1 / sum_dist_white_to_black_licorne) - (1 / sum_dist_black_to_white_licorne));

            double val_aggr = ((1 / dist_aggresive_white - 1 / dist_aggresive_black) * 1.5
                    + (1 / dist_second_aggresive_white - 1 / dist_second_aggresive_black))
                    * HeuristiqueSup.coeff_aggresive;

            double val_def = ((1 / dist_defensive_white - 1 / dist_defensive_black) * 1.5
                    + (1 / dist_second_defensive_white - 1 / dist_second_defensive_black))
                    * HeuristiqueSup.coeff_defensive;

            return val_cp + val_sum_dist + val_aggr + val_def;
        }
    };

    public static Heuristique hbn = new Heuristique() {
        public double eval(PlateauJeu p, Joueur j) {
            return -hbb.eval(p, j);
        }
    };
}