package escampe.heuristique;

import java.util.ArrayList;

import escampe.*;
import escampe.EscampeBoard.Pawn;
import iia.alg.Heuristique;
import iia.modele.*;
import iia.modele.joueur.Joueur;

public class HeuristiqueInf {
    public static Heuristique hbb = new Heuristique() {
        public double eval(PlateauJeu p, Joueur j) {
            EscampeBoard board = (EscampeBoard) p;
            int player_coef;
            if (board.isJb(j)) {
                player_coef = 1;
            } else {
                player_coef = -1;
            }

            if (!board.getBLA()) {
                return Heuristique.INFINITY;
            } else if (!board.getWLA()) {
                return -Heuristique.INFINITY;
            }
            ArrayList<CoupJeu> cp = board.coupsPossibles(j);
            int cp_count = player_coef * cp.size();

            if (cp_count == 1) {
                String coup = ((CoupEscampe) cp.get(0)).getContent();
                // If we have to pass, it's very bad
                if (coup.equals("E")) {
                    cp_count = -20 * player_coef;
                }
            }

            Coord coord_licorne_white = board.getLicorneCoord("blanc");
            Coord coord_licorne_black = board.getLicorneCoord("noir");
            ArrayList<Coord> coords_paladin_white = board.getAllPaladinCoords("blanc");
            ArrayList<Coord> coords_paladin_black = board.getAllPaladinCoords("noir");
            double sum_dist_black_to_white_licorne = 0;
            double sum_dist_white_to_black_licorne = 0;

            for (Coord coord_black : coords_paladin_black) {
                sum_dist_black_to_white_licorne += board.distance_manhattan(coord_black, coord_licorne_white);
            }

            for (Coord coord_white : coords_paladin_white) {
                sum_dist_white_to_black_licorne += board.distance_manhattan(coord_white, coord_licorne_black);
            }
            double val = cp_count + 3 * ((1 / sum_dist_white_to_black_licorne) - (1 / sum_dist_black_to_white_licorne));
            return val;
        }
    };

    public static Heuristique hbn = new Heuristique() {
        public double eval(PlateauJeu p, Joueur j) {
            return -hbb.eval(p, j);
        }
    };
}