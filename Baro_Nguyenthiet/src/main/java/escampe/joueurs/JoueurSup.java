package escampe.joueurs;

import escampe.CoupEscampe;
import escampe.EscampeBoard;
import escampe.IJoueur;
import escampe.heuristique.*;
import iia.alg.*;
import iia.modele.joueur.*;
import iia.modele.*;
import iia.modele.TranspositionUnit.Flag;

import java.util.*;
import java.io.*;

public class JoueurSup implements IJoueur {
    private int myColour;
    private String me;
    private String enemy;
    private EscampeBoard board;
    private AlgoJeu algo;
    private String best_start_white_top = "E1/A2/C2/D1/E2/F2";
    private String best_start_black_top = "E1/B2/C1/D1/E2/F2";
    private String best_start_white_bot = "E6/B5/C6/D5/E5/F5";
    private String best_start_black_bot = "E6/B6/C5/D5/E5/F5";
    private String tt_path = "tt-nguyenthiet";

    public JoueurSup() {
    }

    public void initJoueur(int myColour) {
        this.board = new EscampeBoard();
        this.board.init_zobrist();

        this.myColour = myColour;
        Joueur jmax;
        Joueur jmin;
        Heuristique h;
        if (myColour == -1) {
            this.me = "blanc";
            this.enemy = "noir";
            h = HeuristiqueInf.hbb;
        } else {
            this.me = "noir";
            this.enemy = "blanc";
            h = HeuristiqueInf.hbn;
        }

        jmax = new Joueur(this.me);
        jmin = new Joueur(this.enemy);
        this.algo = new NegAlphaBetaEchecMem(h, jmax, jmin, 9);
        if (myColour == -1) {
            this.board.setJoueur(jmax, jmin);
        } else {
            this.board.setJoueur(jmin, jmax);
        }

        // Attempting to load transposition table
        try {
            FileInputStream fis = new FileInputStream(tt_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ((NegAlphaBetaEchecMem) this.algo).setTt((HashMap<PlateauJeu, TranspositionUnit>) ois.readObject());
            ois.close();
        } catch (Exception e) {
            System.out.println("Failed to load TT");
            e.printStackTrace();
        }

    }

    public int getNumJoueur() {
        return this.myColour;
    }

    public String choixMouvement() {
        if (this.board.gameOver()) {
            return "xxxxx";
        } else {
            if (this.board.getNbTurn() < 2) {
                String coup;
                if (this.me.equals("blanc")) {
                    try {
                        coup = best_start_white_top;
                        this.board.play(coup, this.me);
                    } catch (IllegalArgumentException e) {
                        coup = best_start_white_bot;
                        this.board.play(coup, this.me);
                    }
                } else {
                    try {
                        coup = best_start_black_top;
                        this.board.play(coup, this.me);
                    } catch (IllegalArgumentException e) {
                        coup = best_start_black_bot;
                        this.board.play(coup, this.me);
                    }
                }
                return coup;
            } else {
                long time;
                if (this.board.getNbTurn() <= 10) {
                    time = 1000;
                } else if (this.board.getNbTurn() <= 20) {
                    time = 500;
                } else {
                    time = 1200;
                }
                CoupEscampe coup = (CoupEscampe) algo.meilleurCoup(this.board, time);
                this.board.play(coup.getContent(), this.me);
                return coup.getContent();
            }
        }
    }

    public void declareLeVainqueur(int colour) {
        System.out.println("Temps passé dans NegAlphaBetaEchecMem : "
                + (((NegAlphaBetaEchecMem) this.algo).getTimeSpentTotal()) + "ms");
        System.out.println("Temps passé dans CouPossibleMoves : "
                + (((NegAlphaBetaEchecMem) this.algo).getTimeSpentPossibleMove()) + "ms");
        // System.out.println(((NegAlphaBetaEchecMem) this.algo).getTt());
        // String file_path = "tt-" + (System.nanoTime());
        System.out.println("Sauvegarde de la table de transposition dans : " + tt_path);

        // https://stackoverflow.com/questions/2808277/can-we-write-a-hashtable-to-a-file
        try {
            FileOutputStream fos = new FileOutputStream(tt_path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(((NegAlphaBetaEchecMem) this.algo).getTt());
            oos.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Echec de la sauvegarde de la table de transposition");
        }

        if (colour == this.myColour) {
            System.out.println("Je suis le plus fort");
        } else {
            System.out.println("J'aurai ma vengeance");
        }
    }

    public void mouvementEnnemi(String coup) {
        this.board.play(coup, this.enemy);
    }

    public String binoName() {
        return "Dieu";
    }

    public double getTime() {
        return ((NegAlphaBetaEchecMem) this.algo).getTimeSpentTotal();
    }

}