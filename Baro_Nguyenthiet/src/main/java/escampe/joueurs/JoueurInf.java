package escampe.joueurs;

import escampe.CoupEscampe;
import escampe.EscampeBoard;
import escampe.IJoueur;
import escampe.heuristique.*;
import iia.alg.*;
import iia.modele.joueur.*;
import iia.modele.*;
import java.util.*;

public class JoueurInf implements IJoueur {
    private int myColour;
    private String me;
    private String enemy;
    private EscampeBoard board;
    private AlgoJeu algo;
    private int turn;

    public JoueurInf() {
    }

    public void initJoueur(int myColour) {
        this.board = new EscampeBoard();
        this.myColour = myColour;
        Joueur jmax;
        Joueur jmin;
        Heuristique h;
        if (myColour == -1) {
            this.me = "blanc";
            this.enemy = "noir";
            h = HeuristiqueInf.hbb;
        } else {
            this.me = "noir";
            this.enemy = "blanc";
            h = HeuristiqueInf.hbn;
        }

        jmax = new Joueur(this.me);
        jmin = new Joueur(this.enemy);
        this.algo = new AlphaBeta(h, jmax, jmin, 4);
        if (myColour == -1) {
            this.board.setJoueur(jmax, jmin);
        } else {
            this.board.setJoueur(jmin, jmax);
        }

        this.turn = 0;
    }

    public int getNumJoueur() {
        return this.myColour;
    }

    public String choixMouvement() {
        this.turn++;
        if (this.board.gameOver()) {
            return "xxxxx";
        } else {
            if (turn <= 2) {
                String[] cp = this.board.possibleMoves(this.me);
                Random r = new Random();
                String coup = cp[r.nextInt(cp.length)];
                this.board.play(coup, this.me);
                return coup;
                // this.board.play("F2/B1/C1/D1/F1/A2", this.me);
                // return "F2/B1/C1/D1/F1/A2";
            } else {
                CoupEscampe coup = (CoupEscampe) algo.meilleurCoup(this.board, -1);
                this.board.play(coup.getContent(), this.me);
                return coup.getContent();
            }
        }
    }

    public void declareLeVainqueur(int colour) {
        System.out.println("Temps passé dans AlphaBeta : " + (((AlphaBeta) this.algo).getTimeSpentTotal()) + "ms");
        System.out.println(
                "Temps passé dans CouPossibleMoves : " + (((AlphaBeta) this.algo).getTimeSpentPossibleMove()) + "ms");
        if (colour == this.myColour) {
            System.out.println("Je suis le plus fort");
        } else {
            System.out.println("J'aurai ma vengeance");
        }
    }

    public void mouvementEnnemi(String coup) {
        this.board.play(coup, this.enemy);
        this.turn++;
    }

    public String binoName() {
        return "Être inférieur";
    }

    public double getTime() {
        return ((AlphaBeta) this.algo).getTimeSpentTotal();
    }

}