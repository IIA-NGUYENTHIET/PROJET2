package escampe.joueurs;

import escampe.*;
import java.util.*;

public class JoueurRandom implements IJoueur {

    int myColour;
    String me;
    String enemy;
    EscampeBoard board;
    Random random;
    int turn;

    public JoueurRandom() {
    }

    public void initJoueur(int myColour) {
        this.board = new EscampeBoard();
        this.myColour = myColour;
        this.random = new Random();
        if (this.myColour == -1) {
            this.me = "blanc";
            this.enemy = "noir";
        } else {
            this.me = "noir";
            this.enemy = "blanc";

        }
        turn = 0;
    }

    public int getNumJoueur() {
        return this.myColour;
    }

    public String choixMouvement() {
        this.turn++;
        if (this.board.gameOver()) {
            return "xxxxx";
        } else {
            // if (turn >= 2) {
            String[] cp = this.board.possibleMoves(this.me);
            String coup = cp[random.nextInt(cp.length)];
            this.board.play(coup, this.me);
            return coup;
            // } else {
            // String coup = "E5/A5/D5/B6/C6/F6";
            // this.board.play(coup, this.me);
            // return coup;
            // }
        }
    }

    public void declareLeVainqueur(int colour) {
        if (this.myColour == colour) {
            System.out.println("La chance est avec moi.");
        } else {
            System.out.println("La chance n'est pas avec moi.");
        }
    }

    public void mouvementEnnemi(String coup) {
        this.board.play(coup, this.enemy);
    }

    public String binoName() {
        return "NGUYENTHIET / BARO";
    }

}