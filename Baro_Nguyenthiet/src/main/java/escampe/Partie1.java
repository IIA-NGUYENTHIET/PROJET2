package escampe;

public interface Partie1 {
	
	/**
	 * Initialise un plateau à partir d'un fichier texte
	 * @param filename : le nom du fichier à lire
	 */
	public void setFromFile(String filename);
	
	/**
	 * Sauve la configuration de l'état courant (plateau et pièces restantes)
	 * dans un fichier
	 * @param filename : le nom du fichier à sauvegarder
	 * NB : le format doit être compatible avec celui utlisé pour la lecture
	 */
	public void saveToFile(String filename);
	
	/**
	 * Indique si le coup <move> est valide pour le joueur <player> sur le plateau courant
	 * @param move : le coup à jouer 
	 * 	- sous la forme "B1-D1" en général
	 * 	- sous la forme "C6/A6/B5/D5/E6/F5" pour le coup qui place les pièces
	 * @param player : le joueur qui joue, représenté par "noir" ou "blanc"
	 */
	public boolean isValidMove(String move, String player);
	
	/**
	 * Calcule les coups possibles pour le joueur <player> sur le plateau courant
	 * @param player : le joueur qui joue, représenté par "noir" ou "blanc"
	 */
	public String[] possibleMoves(String player);
	
	/**
	 * Modifie le plateau en jouant le coup avec la pièce choose 
	 * @param move : le coup à jouer, sous la forme "C1-D1" ou "C6/A6/B5/D5/E6/F5"
	 * @param player : le joueur qui joue, représenté par "noir" ou "blanc"
	 */
	public void play(String move, String player);
	
	/**
	 * Renvoie vrai lorsque le plateau correspond à une fin de partie
	 */
	public boolean gameOver();
}
