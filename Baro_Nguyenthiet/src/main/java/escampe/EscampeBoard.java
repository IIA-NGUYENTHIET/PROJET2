
package escampe;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import iia.alg.*;
import iia.modele.*;
import iia.modele.joueur.*;

public class EscampeBoard implements Partie1, PlateauJeu, Serializable {

	// The pawn types
	public static enum Pawn {
		BLACK_PALADIN, BLACK_LICORNE, WHITE_PALADIN, WHITE_LICORNE
	}

	// Pawn type to string
	public String pawnToString(Pawn p) {
		if (p == null) {
			return "-";
		} else {
			switch (p) {
			case BLACK_PALADIN:
				return "n";
			case BLACK_LICORNE:
				return "N";
			case WHITE_PALADIN:
				return "b";
			case WHITE_LICORNE:
				return "B";
			default:
				return "-";
			}
		}
	}

	public int pawnToInt(Pawn p) {
		if (p == null) {
			return 0;
		} else {
			switch (p) {
			case BLACK_PALADIN:
				return 1;
			case BLACK_LICORNE:
				return 2;
			case WHITE_PALADIN:
				return 3;
			case WHITE_LICORNE:
				return 4;
			default:
				return -1;
			}
		}
	}

	// Get a pawn from a char
	public Pawn charToPawn(char c) {
		switch (c) {
		case 'N':
			return Pawn.BLACK_LICORNE;
		case 'n':
			return Pawn.BLACK_PALADIN;
		case 'B':
			return Pawn.WHITE_LICORNE;
		case 'b':
			return Pawn.WHITE_PALADIN;
		default:
			return null;
		}
	}

	// Check the Pawn is a Licorne
	public boolean isLicorne(Pawn p) {
		if (p == null) {
			return false;
		} else {
			switch (p) {
			case BLACK_LICORNE:
				return true;
			case WHITE_LICORNE:
				return true;
			default:
				return false;
			}
		}
	}

	// Check the Pawn is a Paladin
	public boolean isPaladin(Pawn p) {
		return !isLicorne(p);
	}

	// Some regexp to know valid moves
	private final Pattern standardMovePattern = Pattern.compile("[A-F][1-6]-[A-F][1-6]");
	private final Pattern startMovePattern = Pattern.compile("([A-F][1-6]/){5}[A-F][1-6]");

	// Instances variables
	// The layout of the cells
	private final int[][] layout;
	// The grid of the pawns
	private Pawn[][] pawns;
	// white and black player
	private Joueur jb;
	private Joueur jn;
	// Represents the last cell that was stepped on
	private int last_cell;
	// Some boolean for faster access
	private boolean black_licorne_alive;
	private boolean white_licorne_alive;
	// Number of turn
	private int nb_turn;

	public EscampeBoard() {
		this.layout = new int[][] { { 1, 2, 2, 3, 1, 2 }, { 3, 1, 3, 1, 3, 2 }, { 2, 3, 1, 2, 1, 3 },
				{ 2, 1, 3, 2, 3, 1 }, { 1, 3, 1, 3, 1, 2 }, { 3, 2, 2, 1, 3, 2 } };
		this.pawns = new Pawn[6][6];
		this.last_cell = 0;
		this.black_licorne_alive = true;
		this.white_licorne_alive = true;
		this.nb_turn = 0;
	}

	public EscampeBoard(EscampeBoard bis) {
		this.layout = new int[][] { { 1, 2, 2, 3, 1, 2 }, { 3, 1, 3, 1, 3, 2 }, { 2, 3, 1, 2, 1, 3 },
				{ 2, 1, 3, 2, 3, 1 }, { 1, 3, 1, 3, 1, 2 }, { 3, 2, 2, 1, 3, 2 } };
		this.pawns = new Pawn[6][6];
		this.last_cell = bis.getLastCell();
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				this.setPawn(i, j, bis.getPawn(i, j));
			}
		}
		this.jb = bis.getJB();
		this.jn = bis.getJN();
		this.black_licorne_alive = bis.getBLA();
		this.white_licorne_alive = bis.getWLA();
		this.nb_turn = bis.getNbTurn();
	}

	// Getters
	// Return the cell at col (x) and line (y)
	public int getCell(int x, int y) {
		return this.layout[y][x];
	}

	// Return the pawn at col (x) and line (y)
	public Pawn getPawn(int x, int y) {
		return this.pawns[y][x];
	}

	public int getLastCell() {
		return this.last_cell;
	}

	public int getNbTurn() {
		return this.nb_turn;
	}

	public Pawn getPawnFromCoord(Coord coord) {
		// Fetch the coordinates
		return this.getPawn(coord.x, coord.y);
	}

	/**
	 * Return the position of all the pawn that is equal to the enum specified in
	 * the parameters
	 */
	public ArrayList<Coord> getAllCoordOfPawn(Pawn p) {
		ArrayList<Coord> res = new ArrayList<>();
		for (int x = 0; x < this.pawns.length; x++) {
			for (int y = 0; y < this.pawns[x].length; y++) {
				Pawn pbis = this.getPawn(x, y);
				if (pbis != null && this.getPawn(x, y).equals(p)) {
					res.add(new Coord(x, y));
				}
			}
		}
		return res;
	}

	/**
	 * Return the coordinates of all the paladin of a given player specified in the
	 * parameters
	 */
	public ArrayList<Coord> getAllPaladinCoords(String player) {
		if (player.equals("blanc")) {
			return getAllCoordOfPawn(Pawn.WHITE_PALADIN);
		} else {
			return getAllCoordOfPawn(Pawn.BLACK_PALADIN);
		}
	}

	/**
	 * Returns the coordinate of the licorne, there should be only one, or null if
	 * there's none.
	 */
	public Coord getLicorneCoord(String player) {
		Pawn p;
		if (player.equals("blanc")) {
			p = Pawn.WHITE_LICORNE;
		} else {
			p = Pawn.BLACK_LICORNE;
		}
		ArrayList<Coord> res = this.getAllCoordOfPawn(p);
		if (res.size() >= 1) {
			return res.get(0);
		} else {
			return null;
		}
	}

	public boolean getBLA() {
		return this.black_licorne_alive;
	}

	public boolean getWLA() {
		return this.white_licorne_alive;
	}

	public Joueur getJB() {
		return this.jb;
	}

	public Joueur getJN() {
		return this.jn;
	}

	// Returns the manhattan distance between two point
	public int distance_manhattan(Coord a, Coord b) {
		return Math.abs(b.x - a.y) + Math.abs(b.y - a.y);
	}

	// Setters
	// Set the pawn at col (x) and line (y)
	public void setPawn(int x, int y, Pawn p) {
		this.pawns[y][x] = p;
	}

	public void setLastCell(int n) {
		if (n >= 0 && n <= 3) {
			this.last_cell = n;
		}
	}

	public void setJoueur(Joueur b, Joueur n) {
		this.jb = b;
		this.jn = n;
	}

	// Other methods
	public boolean isJb(Joueur j) {
		return this.jb.equals(j);
	}

	public boolean isJn(Joueur j) {
		return this.jn.equals(j);
	}

	public String getJoueurEscampe(Joueur j) {
		if (this.isJb(j)) {
			return "blanc";
		} else {
			return "noir";
		}
	}

	// Partie1 methods
	@Override
	public void setFromFile(String filename) {
		// The array that contains the file
		char[][] tab = new char[9][];

		// Read the file and fill the array
		try {
			FileReader fileReader = new FileReader(filename);
			for (int i = 0; i < tab.length; i++) {
				tab[i] = new char[12];
				if (fileReader.read() != -1) {
					fileReader.read(tab[i]);
				}
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Convert the char to a game board
		for (int i = 1; i < tab.length - 2; i++) {
			for (int j = 2; j <= 7; j++) {
				this.pawns[i - 1][j - 2] = this.charToPawn(tab[i][j]);
			}
		}

		// Get the last cell
		this.last_cell = (int) tab[8][1];
	}

	public String toString() {
		String res = "";
		res = "%  ABCDEF   \n";
		for (int line = 0; line < 6; line++) {
			res += "0" + (line + 1) + " ";
			for (int col = 0; col < 6; col++) {
				res += pawnToString(this.getPawn(col, line));
			}
			res += " 0" + (line + 1) + "\n";
		}
		res += "%  ABCDEF   \n";
		res += "%" + this.last_cell;
		return res;
	}

	@Override
	public void saveToFile(String filename) {
		try {
			PrintWriter out = new PrintWriter(filename);
			out.print(this.toString());
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static boolean pawnIsOwnedBy(Pawn pawn, String player) {
		if (pawn == null) {
			return false;
		} else if (player.equals("blanc")) {
			// White player
			if (!(pawn.equals(Pawn.WHITE_LICORNE) || pawn.equals(Pawn.WHITE_PALADIN))) {
				return false;
			} else {
				return true;
			}

		} else if (player.equals("noir")) {
			// Black player
			if (!(pawn.equals(Pawn.BLACK_LICORNE) || pawn.equals(Pawn.BLACK_PALADIN))) {
				return false;
			} else {
				return true;
			}

		} else {
			// not black or white
			return false;
		}
	}

	// The dst cell is free if there's no one
	private boolean dstCellIsFree(Coord coord) {
		return getPawnFromCoord(coord) == null;
	}

	// The dst cell has an enemy licorne
	private boolean dstCellIsEnemyLicorne(Coord coord, String player) {
		Pawn dst_pawn = getPawnFromCoord(coord);
		return (!pawnIsOwnedBy(dst_pawn, player)) && isLicorne(dst_pawn);
	}

	// Fetch the neighbouring cells
	private Collection<Coord> neighboursCell(Coord current, String player) {
		Collection<Coord> res = new LinkedList<>();
		for (int x = -1; x < 2; x++) {
			for (int y = -1; y < 2; y++) {
				if (x == 0 ^ y == 0) {
					int neighbour_x = current.x + x;
					int neighbour_y = current.y + y;
					if (0 <= neighbour_x && neighbour_x <= 5 && 0 <= neighbour_y && neighbour_y <= 5) {
						res.add(new Coord(neighbour_x, neighbour_y));
					}
				}
			}
		}
		return res;
	}

	// Checks if there is a path
	private HashMap<Coord, Coord> pathToDst(Coord start, Coord goal, String player) {
		List<Coord> dejaDev = new LinkedList<>();
		Queue<Coord> frontiere = new LinkedList<>();

		// The data structure that contains the path
		HashMap<Coord, Coord> pere = new HashMap<>();

		frontiere.add(start);
		pere.put(start, null);

		while (!frontiere.isEmpty()) {
			Coord current = frontiere.poll();
			// Goal is found, return the DS with path
			if (current.equals(goal)) {
				return pere;
			} else {
				dejaDev.add(current);
				Collection<Coord> successeurs = neighboursCell(current, player);
				for (Coord succ : successeurs) {
					// Condition for nodes to be considered
					if (!(dejaDev.contains(succ) || frontiere.contains(succ)) && ((dstCellIsFree(succ))
							|| ((goal.equals(succ)) && dstCellIsEnemyLicorne(succ, player)))) {
						frontiere.add(succ);
						pere.put(succ, current);
					}
				}
			}
		}

		// Goal is not found
		return null;
	}

	// Check if the path from start to goal is of length length
	private boolean pathSameLength(HashMap<Coord, Coord> path, int length, Coord start, Coord goal) {
		Coord tmp = goal;
		int cpt = 0;
		while (!tmp.equals(start)) {
			tmp = path.get(tmp);
			cpt++;
		}
		return cpt == length;
	}

	// Check standard move, eg B1-C1
	private boolean isValidMoveStandard(Coord[] coords, String player) {
		// Check if there's two coordinates, if not, false move
		if (coords == null || coords.length != 2) {
			return false;
		} else {
			Pawn pawn = getPawn(coords[0].x, coords[0].y);
			if (!pawnIsOwnedBy(pawn, player)) {
				// First, verify that the pawn is owned by the player
				return false;
			} else if ((!dstCellIsFree(coords[1]))
					&& (!(!isLicorne(pawn) && dstCellIsEnemyLicorne(coords[1], player)))) {
				// Then check if the dst cell is free or is an enemy licorne
				// If it is a licorn we have to make sure the friendly pawn
				// is not a licorn
				return false;
			} else if (this.last_cell != 0 && this.getCell(coords[0].x, coords[0].y) != this.last_cell) {
				// Then check if the lisere from src correspond with the last played cell
				return false;
			} else {
				// Then check if there's a path
				HashMap<Coord, Coord> path = pathToDst(coords[0], coords[1], player);
				if (path == null) {
					return false;
				} else {
					// If there is a path then check if the path is reachable with n steps from the
					// liseres
					int length = this.getCell(coords[0].x, coords[0].y);
					if (pathSameLength(path, length, coords[0], coords[1])) {
						return true;
					} else {
						// there is no path of the exact length, but if it's a tile of 3, it doesnt have
						// to take the most optimal path
						if (length != 3) {
							return false;
						} else {
							int diff_x = coords[0].x - coords[1].x;
							int diff_y = coords[0].y - coords[1].y;
							// diangonal and not move to neighbour tile is not ok
							if ((Math.abs(diff_x) > 1) || (Math.abs(diff_y) > 1)
									|| (Math.abs(diff_x) == Math.abs(diff_y))) {
								return false;
							} else {
								// The number of blockers, if more than 2 it's impossible
								// There are two path, either go to left or to right. if left and right are
								// blocked, it's impossible
								boolean not_blocked_left = true;
								boolean not_blocked_right = true;

								for (int i = -1; i < 2; i++) {
									for (int j = 0; j < 2; j++) {
										int block_x = diff_y * i + coords[0].x - diff_x * j;
										int block_y = diff_x * i + coords[0].y - diff_y * j;
										Pawn p;
										try {
											p = getPawn(block_x, block_y);
										} catch (ArrayIndexOutOfBoundsException e) {
											// It doesn't matter which color, we will pretend like there is
											// a pawn there and we cant get past it
											p = Pawn.BLACK_PALADIN;
										}
										if ((block_x != coords[0].x || block_y != coords[0].y) && p != null) {
											if (i < 0) {
												not_blocked_left = false;
											}
											if (i > 0) {
												not_blocked_right = false;
											}
										}
									}
								}
								return not_blocked_left || not_blocked_right;
							}
						}
					}
				}
			}
		}
	}

	// Check if there are no duplicate in the coords array
	private boolean noDuplicate(Coord[] coords) {
		for (int i = 0; i < coords.length - 1; i++) {
			for (int j = i + 1; j < coords.length; j++) {
				if (coords[i].equals(coords[j])) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean isValidMoveStart(Coord[] coords, String player) {
		if (coords == null || coords.length != 6) {
			return false;
		} else {
			// First check if there are no duplicates
			if (!noDuplicate(coords)) {
				return false;
			} else {
				boolean top_available = true;
				boolean bot_available = true;

				// Check top
				top: for (int i = 0; i < 6; i++) {
					for (int j = 0; j < 2; j++) {
						Pawn p = this.getPawn(i, j);
						if (p != null) {
							top_available = false;
							break top;
						}
					}
				}

				// Check bottom
				bot: for (int i = 0; i < 6; i++) {
					for (int j = 4; j < 6; j++) {
						Pawn p = this.getPawn(i, j);
						if (p != null) {
							bot_available = false;
							break bot;
						}
					}
				}

				// For each coordinates, we check if there is already someone
				for (Coord coord : coords) {
					// bot is unavailable and there is pawn in bot
					if (!bot_available && coord.y >= 4) {
						return false;
					} else if (!top_available && coord.y <= 1) {
						return false;
					} else {
						// we put once in the top, we put everytime in the top for the rest
						if (coord.y >= 4) {
							top_available = false;
							// same but with bot
						} else if (coord.y <= 1) {
							bot_available = false;
							// in the middle, not ok
						} else {
							return false;
						}

						// Pawn already exist
						Pawn p = this.getPawn(coord.x, coord.y);
						if (p != null) {
							return false;
						}
					}
				}

				return true;
			}
		}
	}

	// We make the assumtpion that the board is valid before calling this function
	@Override
	public boolean isValidMove(String move, String player) {
		// Regexp to see if the move matches the format
		Matcher mStandard = standardMovePattern.matcher(move);
		Matcher mStart = startMovePattern.matcher(move);

		// The move is a standard move
		if (mStandard.matches()) {
			String[] tmp = move.split("-");
			Coord[] coords = new Coord[tmp.length];
			for (int i = 0; i < tmp.length; i++) {
				coords[i] = new Coord(tmp[i]);
			}
			return isValidMoveStandard(coords, player);

		}
		// The move is a start move
		else if (mStart.matches()) {
			String[] tmp = move.split("/");
			Coord[] coords = new Coord[tmp.length];
			for (int i = 0; i < tmp.length; i++) {
				coords[i] = new Coord(tmp[i]);
			}

			return isValidMoveStart(coords, player);
		}

		// The move doesnt not match the regexp
		else {
			return move.equals("E");
		}
	}

	// Idea : enumerate then check if it's valid
	@Override
	public String[] possibleMoves(String player) {
		List<String> res = new LinkedList<>();
		// Check for standard move

		if (this.nb_turn >= 2) {
			for (int x1 = 0; x1 < 6; x1++) {
				for (int y1 = 0; y1 < 6; y1++) {
					for (int x2 = 0; x2 < 6; x2++) {
						for (int y2 = 0; y2 < 6; y2++) {
							String src = new Coord(x1, y1).toString();
							String dst = new Coord(x2, y2).toString();
							String tmp = src + "-" + dst;
							if (isValidMove(tmp, player)) {
								res.add(tmp);
							}
						}
					}
				}
			}
		} else {
			// Check for start move
			for (int i = 0; i < 2; i++) {
				for (int licorne = 0; licorne < 12; licorne++) {
					for (int p0 = 0; p0 < 12 - 4; p0++) {
						for (int p1 = p0 + 1; p1 < 12 - 3; p1++) {
							for (int p2 = p1 + 1; p2 < 12 - 2; p2++) {
								for (int p3 = p2 + 1; p3 < 12 - 1; p3++) {
									for (int p4 = p3 + 1; p4 < 12; p4++) {
										if (licorne != p1 && licorne != p2 && licorne != p3 && licorne != p4) {
											Coord[] coords = new Coord[6];
											coords[0] = new Coord(licorne % 6, licorne / 6 + i * 4);
											coords[1] = new Coord(p0 % 6, p0 / 6 + i * 4);
											coords[2] = new Coord(p1 % 6, p1 / 6 + i * 4);
											coords[3] = new Coord(p2 % 6, p2 / 6 + i * 4);
											coords[4] = new Coord(p3 % 6, p3 / 6 + i * 4);
											coords[5] = new Coord(p4 % 6, p4 / 6 + i * 4);

											if (isValidMoveStart(coords, player)) {
												String move = coords[0].toString();
												for (int k = 1; k < 6; k++) {
													move += "/" + coords[k].toString();
												}
												res.add(move);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (res.size() == 0) {
			return new String[] { "E" };
		} else {
			return res.toArray(new String[res.size()]);
		}
	}

	@Override
	public void play(String move, String player) {
		// Check if the move is valid
		if (!isValidMove(move, player)) {
			System.out.println("ERROR : " + move + " for " + player + " is NOT a valid move!");
			System.out.println(this);
			throw new IllegalArgumentException();
		} else {
			// Increment the number of turn
			this.nb_turn++;
			// Check if it's not pass
			if (move.equals("E")) {
				this.last_cell = 0;
			} else {

				// Split the move into coordinates
				String coords_start_string[] = move.split("/");
				String coords_standard_string[] = move.split("-");

				// Check start move
				if (coords_start_string.length == 6) {
					// Fetch the coords
					Coord[] coords_start = new Coord[6];
					for (int i = 0; i < 6; i++) {
						coords_start[i] = new Coord(coords_start_string[i]);
					}

					// Select which pawn to spawn
					Pawn licorne;
					Pawn paladin;
					if (player.equals("noir")) {
						licorne = Pawn.BLACK_LICORNE;
						paladin = Pawn.BLACK_PALADIN;
					} else {
						licorne = Pawn.WHITE_LICORNE;
						paladin = Pawn.WHITE_PALADIN;
					}

					// Spawn them
					this.setPawn(coords_start[0].x, coords_start[0].y, licorne);
					for (int i = 1; i < coords_start.length; i++) {
						this.setPawn(coords_start[i].x, coords_start[i].y, paladin);
					}

					// Check standard move
				} else if (coords_standard_string.length == 2) {
					Coord[] coords_standard = new Coord[2];
					for (int i = 0; i < 2; i++) {
						coords_standard[i] = new Coord(coords_standard_string[i]);
					}

					Pawn p = this.getPawn(coords_standard[0].x, coords_standard[0].y);
					// Remove the pawn
					this.setPawn(coords_standard[0].x, coords_standard[0].y, null);
					// Replace it somewher else
					// Check if there is licorne
					Pawn pbis = this.getPawn(coords_standard[1].x, coords_standard[1].y);
					if (pbis == Pawn.BLACK_LICORNE) {
						this.black_licorne_alive = false;
					} else if (pbis == Pawn.WHITE_LICORNE) {
						this.white_licorne_alive = false;
					}
					this.setPawn(coords_standard[1].x, coords_standard[1].y, p);

					// Update the last cell played
					this.last_cell = this.getCell(coords_standard[1].x, coords_standard[1].y);
				}
			}
		}
	}

	@Override
	public boolean gameOver() {
		boolean black_present = false;
		boolean white_present = false;
		boolean black = true, white = true;
		for (int i = 0; i < this.pawns.length; i++) {
			for (int j = 0; j < this.pawns[i].length; j++) {
				if (this.getPawn(i, j) == Pawn.BLACK_LICORNE) {
					black = false;
					black_present = true;
				} else if (this.getPawn(i, j) == Pawn.WHITE_LICORNE) {
					white = false;
					white_present = true;
				} else if (this.getPawn(i, j) == Pawn.BLACK_PALADIN) {
					black_present = true;
				} else if (this.getPawn(i, j) == Pawn.WHITE_PALADIN) {
					white_present = true;
				}
			}
		}
		boolean is_start = !black_present || !white_present;
		return (!is_start) && (black || white);
	}

	// Partie implementation de PlateauJeu
	@Override
	public ArrayList<CoupJeu> coupsPossibles(Joueur j) {
		String player = this.getJoueurEscampe(j);
		String[] tmp = this.possibleMoves(player);
		ArrayList<CoupJeu> res = new ArrayList<>();

		for (String c : tmp) {
			res.add(new CoupEscampe(c));
		}

		return res;
	}

	@Override
	public void joue(Joueur j, CoupJeu c) {
		CoupEscampe ce = (CoupEscampe) c;
		String player = this.getJoueurEscampe(j);
		this.play(ce.getContent(), player);
	}

	@Override
	public boolean finDePartie() {
		return gameOver();
	}

	@Override
	public PlateauJeu copy() {
		return new EscampeBoard(this);
	}

	@Override
	public boolean coupValide(Joueur j, CoupJeu c) {
		CoupEscampe ce = (CoupEscampe) c;
		String player = this.getJoueurEscampe(j);
		return this.isValidMove(ce.getContent(), player);
	}

	public static void main(String[] args) {
		EscampeBoard board = new EscampeBoard();

		// pose noir
		String[] pm1 = board.possibleMoves("noir");
		System.out.println("Il y a " + pm1.length + " combinaisons de coup possibles au début.");
		System.out.println("");
		System.out.println("On pose quelque pion noirs...");
		board.play(pm1[0], "noir");
		System.out.println(board);

		// pose blanc
		String[] pm2 = board.possibleMoves("blanc");
		System.out.println("Il y a " + pm2.length + " combinaisons de coup possibles au début.");
		System.out.println("");
		System.out.println("On pose quelque pion blancs...");
		board.play(pm2[0], "blanc");
		System.out.println(board);
	}

	// Zobirst encoding
	public static int[][] board_zobrist;
	public static int[] last_lisere_zobrist;

	public void init_zobrist() {
		Random r = new Random(123);
		board_zobrist = new int[36][5];
		for (int i = 0; i < 36; i++) {
			for (int j = 0; j < 5; j++) {
				board_zobrist[i][j] = r.nextInt() & Integer.MAX_VALUE;
			}
		}
		last_lisere_zobrist = new int[4];
		for (int i = 0; i < 4; i++) {
			last_lisere_zobrist[i] = r.nextInt() & Integer.MAX_VALUE;
		}
	}

	@Override
	public int hashCode() {
		int h = 0;
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				Pawn p = this.getPawn(i, j);
				int n = this.pawnToInt(p);
				h ^= board_zobrist[i * 6 + j][n];
			}
		}
		h ^= last_lisere_zobrist[this.last_cell];
		return h;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (!(o instanceof EscampeBoard)) {
			return false;
		}

		EscampeBoard eb = (EscampeBoard) o;
		if (!(this.last_cell == eb.getLastCell())) {
			return false;
		}

		if (!this.isJb(eb.getJB())) {
			return false;
		}

		if (!this.isJn(eb.getJN())) {
			return false;
		}

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (!(this.getPawn(i, j) == eb.getPawn(i, j))) {
					return false;
				}
			}
		}

		return true;
	}

}
