package escampe;

public class Coord {
    public int x;
    public int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coord(String s) {
        if (s.length() != 2) {
            throw new IllegalArgumentException();
        } else {
            this.x = s.charAt(0) - 'A';
            this.y = s.charAt(1) - '1';
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Coord)) {
            return false;
        }

        final Coord other = (Coord) obj;
        return this.y == other.y && this.x == other.x;
    }

    @Override
    public int hashCode() {
        return 10 * this.x + this.y;
    }

    @Override
    public String toString() {
        String res = "";
        int col = 'A' + this.x;
        int lin = '1' + this.y;
        res += Character.toString((char) col);
        res += Character.toString((char) lin);
        return res;
    }
}
